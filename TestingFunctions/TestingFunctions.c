
#include "TestingFunctions.h"
#include "FifoBuffer/FifoBuffer.h"

struct fifoBufferElement fifoBuffer;
unsigned int packetInfoLost = 0;

void InitTestingFunctions(void)
{
	InitFifoBuffer(&fifoBuffer);
}

void AnalyzePacket(struct pl_data* packet_data)
{
   unsigned int testingElement = 0;
   if(packet_data->functionId == TEST)
   {
	   return;
   }
   //else
   //{
	   usleep(500);
	   if(GetEmtyPlacesInFifoBuffer(&fifoBuffer) >= 1)
	   {
		   testingElement |= packet_data->dataLength & 0x000000FF;
		   if(packet_data->packetType == PL_PTYPE_ERR)
		   {
			 testingElement |= (packet_data->operand[0]<<8) & 0x00000F00;
		   }
		   testingElement |= (packet_data->functionId<<12)& 0x0000F000;
		   testingElement |= (packet_data->priority<<16) & 0x00FF0000;
		   testingElement |= (packet_data->clientId<<24) & 0x0F000000;
		   testingElement |= (packet_data->packetType<<28) & 0xF0000000;
		   PutElementInFifoBuffer(&fifoBuffer, &testingElement);
		   //printf("TestingEntry %x\n", testingElement);
	   }
	   else
	   {
		   packetInfoLost = 1;

	//#ifdef DEBUG
		   //printf("Buffer Full %i\n", GetEmtyPlacesInFifoBuffer(&fifoBuffer));
	//#endif
	   }
   //}
}

void BuildTestingResponse(struct pl_data* packet_data)
{
  unsigned int elementsInBuffer;
  unsigned int testingElement;
  unsigned int loopCount = 0;
  elementsInBuffer = GetElementsInFifoBuffer(&fifoBuffer);
  //printf("Elements in Buffer %i, empty: %i\n", elementsInBuffer, GetEmtyPlacesInFifoBuffer(&fifoBuffer));
  pl_create_response(packet_data);
  if(packetInfoLost != 0)
  {
	  packet_data->priority = 0xFF;
  }
  if(elementsInBuffer >= PL_OPERAND_COUNT / 2)
  {
	  packet_data->dataLength = PL_OPERAND_COUNT;
	  for(loopCount = 0; loopCount < PL_OPERAND_COUNT / 2; loopCount++)
	  {
		  GetElementFromFifoBuffer(&fifoBuffer, &testingElement);
		  //printf("TestingEntry %x length %i\n", testingElement, packet_data->dataLength);
		  packet_data->operand[loopCount*2] = (testingElement>>16) & 0xFFFF;
		  packet_data->operand[(loopCount*2)+1] = (testingElement) & 0xFFFF;
	  }
  }
  else if(elementsInBuffer > 1)
  {
	  packet_data->dataLength = elementsInBuffer*2;
	  for(loopCount = 0; loopCount < elementsInBuffer; loopCount++)
	  	  {
	  		  GetElementFromFifoBuffer(&fifoBuffer, &testingElement);
	  		 //printf("TestingEntry %x length %i\n", testingElement, packet_data->dataLength);
	  		  packet_data->operand[loopCount*2] = (testingElement>>16) & 0xFFFF;
	  		  packet_data->operand[(loopCount*2)+1] = (testingElement) & 0xFFFF;
	  	  }
  }
  else
  {

	  packet_data->dataLength = 0;
#ifdef DEBUG
	  // printf("Buffer Empty\n");
#endif
  }
}

