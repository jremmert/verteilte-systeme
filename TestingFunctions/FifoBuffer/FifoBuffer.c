#include "FifoBuffer.h"
#include "stdio.h"
/*
struct fifoBufferElement
{
	unsigned int putPointer;
	unsigned int getPointer;
	unsigned int buffer[ELEMENT_COUNT];
};
*/
int InitFifoBuffer(struct fifoBufferElement* buffer)
{
	buffer->getPointer = 0;
	buffer->putPointer = 0;
	return FIFO_NO_ERROR;
}
unsigned int GetElementsInFifoBuffer(struct fifoBufferElement* buffer)
{
	if(buffer->putPointer >= buffer->getPointer)
	{
		return buffer->putPointer - buffer->getPointer;
	}
	else
	{
		return ELEMENT_COUNT - (buffer->getPointer - buffer->putPointer);
	}
}
unsigned int GetEmtyPlacesInFifoBuffer(struct fifoBufferElement* buffer)
{
	if(buffer->putPointer >= buffer->getPointer)
		{
			return ELEMENT_COUNT - (buffer->putPointer - buffer->getPointer) -1;
		}
		else
		{
			return buffer->getPointer - buffer->putPointer - 1;
		}
}
int PutElementInFifoBuffer(struct fifoBufferElement* buffer, unsigned int* element)
{
	if(GetEmtyPlacesInFifoBuffer(buffer) >= 1)
	{
		buffer->buffer[buffer->putPointer] = *element;
		buffer->putPointer++;
		if(buffer->putPointer >= ELEMENT_COUNT)buffer->putPointer = 0;
		return FIFO_NO_ERROR;
	}
	else
	{
		//printf("Fifo Full!\n");
		return FIFO_ERROR;
	}
}
int GetElementFromFifoBuffer(struct fifoBufferElement* buffer, unsigned int* element)
{
	if(GetElementsInFifoBuffer(buffer) >= 1)
		{
			 *element = buffer->buffer[buffer->getPointer];
			buffer->getPointer++;
			if(buffer->getPointer >= ELEMENT_COUNT)buffer->getPointer = 0;
			return FIFO_NO_ERROR;
		}
		else
		{
			return FIFO_ERROR;
		}
}
