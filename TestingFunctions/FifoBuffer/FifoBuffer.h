#if !defined _fifobuffer_h_
#define _fifobuffer_h_

#define ELEMENT_COUNT    2000
#define FIFO_NO_ERROR    0
#define FIFO_ERROR       1

struct fifoBufferElement
{
	unsigned int putPointer;
	unsigned int getPointer;
	unsigned int buffer[ELEMENT_COUNT];
};

int InitFifoBuffer(struct fifoBufferElement*);
unsigned int GetElementsInFifoBuffer(struct fifoBufferElement*);
unsigned int GetEmtyPlacesInFifoBuffer(struct fifoBufferElement*);
int PutElementInFifoBuffer(struct fifoBufferElement*, unsigned int*);
int GetElementFromFifoBuffer(struct fifoBufferElement*, unsigned int*);



#endif
