/**
 *	\file TestingFunctions.h
 *	\brief Server side Functions to support testing
 *	\author Armin Failenschmid
 *	\version 0.9
 *
 */
#if !defined _testingfunctions_h_
#define _testingfunctions_h_

#include "../server/includes.h"
#include "../packetlib/packetlib.h"


//Muss gestartet werden, damit die weiteren Funktionen verwendet werden können
void InitTestingFunctions(void);

//jedes empfangene und gesendete Paket muss an diese Funktion übergeben werden
//Diese liest die Informationen aus und speichert sie im FIFO ab
void AnalyzePacket(struct pl_data*);

//Diese Funktion baut das Antwortpaket für die functionId "TEST" zusammen
void BuildTestingResponse(struct pl_data*);







#endif
