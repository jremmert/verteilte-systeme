#!/bin/bash

clientID=37
priority=10
IN_File="/home/labuser/verteilte-systeme/doku/text.bin"
OUT_File="/home/labuser/verteilte-systeme/doku/out.txt"
SERVER_COUNT=2
rm /home/labuser/verteilte-systeme/doku/out.txt
echo out.txt removed.. start Client
cd /home/labuser/eclipse/workspace/vs_client/Debug/

./vs_client $clientID $priority $IN_File $OUT_File $SERVER_COUNT

diff -s -q /home/labuser/verteilte-systeme/doku/out.txt /home/labuser/verteilte-systeme/doku/out_reference.txt

#less /home/labuser/verteilte-systeme/doku/out.txt

# | tee /home/labuser/verteilte-systeme/out.txt // pipe output to txt-file on host
#
