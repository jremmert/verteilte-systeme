#!/bin/bash

FileType=2

clientID=99
priority=200
IN_File1="~/text2.bin"
IN_File2="~/de_bello_gallico2.bin"
OUT_File="~/out2.txt"
REFERENCE_FILE1="~/out_reference.txt"
REFERENCE_FILE2="~/de_bello_gallico_reference.txt"
SERVER_COUNT=15
export SSHPASS="vslab"

cp /home/labuser/eclipse/workspace/vs_client/Debug/vs_client /home/labuser/eclipse/workspace/vs_client/Debug/vs_client1

sshpass -e scp -oBatchMode=no /home/labuser/eclipse/workspace/vs_client/Debug/vs_client1 grupped@141.47.75.250:~/

if [ ${FileType} -eq 2 ];
then
	date
	sshpass -e scp -oBatchMode=no /home/labuser/verteilte-systeme/doku/de_bello_gallico.bin grupped@141.47.75.250:~/de_bello_gallico2.bin
fi
if [ ${FileType} -eq 1 ];
then 
	date
	sshpass -e scp -oBatchMode=no /home/labuser/verteilte-systeme/doku/text2.bin grupped@141.47.75.250:~/ 
fi

sshpass -e scp -oBatchMode=no /home/labuser/verteilte-systeme/doku/out_reference.txt grupped@141.47.75.250:~/
echo "vs_client & text.bin & out_reference.txt copied"

sshpass -e ssh -t -l -oBatchMode=no grupped@141.47.75.250 "rm $OUT_File"
echo "out.txt removed"

if [ ${FileType} = 2 ];
then 
	sshpass -e ssh -t -l -oBatchMode=no grupped@141.47.75.250 "cd ~/; mv vs_client1 vslabtest; chmod +x vslabtest; ./vslabtest $clientID $priority $IN_File2 $OUT_File $SERVER_COUNT"
       echo "vs_client started"
fi
if [ ${FileType} = 1 ]; 
then 
	sshpass -e ssh -t -l -oBatchMode=no grupped@141.47.75.250 "cd ~/; mv vs_client1 vslabc; chmod +x vslabc; ./vslabc $clientID $priority $IN_File1 $OUT_File $SERVER_COUNT"
	echo "vs_client started"
fi

if [ ${FileType} = 1 ];
then
    sshpass -e ssh -t -l -oBatchMode=no grupped@141.47.75.250 "diff -s -q  $OUT_File $REFERENCE_FILE1"
fi

if [ ${FileType} = 2 ];
then
    sshpass -e ssh -t -l -oBatchMode=no grupped@141.47.75.250 "diff -s -q  $OUT_File $REFERENCE_FILE2"
fi