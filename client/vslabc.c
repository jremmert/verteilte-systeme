/**
 *  \file vslabc.c
 *  \brief The VSLab client
 *  \author Marc Juettner, marc.juettner@juettner-itconsult.de
 *  \author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *  \version 1.0
 *
 *  \defgroup vslabclient VSLab client implementation
 *  \{
 */
#include "includes.h"
#include "sys/time.h"




int main(int argc, char **argv)
{
	int iReturn = 0;
	int serverCount;
	struct timeval begin, end;
	gettimeofday(&begin, NULL);

	char *filePathIn, *filePathOut;

	if (argc < 5) {
		printf("Missing/Wrong arguments!\n");
		printf("Usage: vslabc ID PRIO PATHIN PATHOUT SERVERCOUNT \n");
		return -1;
	}

	// convert command line parameters to integers
	vslcl_SetClientID(atoi(argv[1]));
	vslcl_SetPrio(atoi(argv[2]));
	filePathIn = argv[3];
	filePathOut = argv[4];
	serverCount=atoi(argv[5]);
	if(serverCount > 15){
		printf("Max server count is 15, starting with 15 servers\n");
		serverCount=15;
	}
	vslcl_SetServercount(serverCount);

	//set printf to unbuffered output
	setbuf(stdout, NULL);


	// introduce yourself...
	printf("VSLab client, version %s, build %s %s\n", VSLC_VERSION, __DATE__, __TIME__);

	// check command line parameters

	iReturn = vslcl_OpenFile(filePathIn, filePathOut);

	if (iReturn)
	{
		printf("VSLab client: Can't open Files\n");
		return -1;
	}

	printf("Client starting decryption with a serverCount of: %d\n",serverCount);

	iReturn = vslcl_ExtractGeneratorPolynom();

	if (iReturn == EOF)
	{
		printf("VSLab client: EOF during GP reading\n");
		return -1;
	}

	iReturn = vslcl_Open();

	if (iReturn) {
		printf("VSLab client: Can't init Sockets\n");
		return -1;
	}


    iReturn = vslcl_Init_server();
	if (iReturn) {
		printf("VSLab client: Can't init Servers \n");
		return -1;
	}

	iReturn = vslcl_FeedServer();

	if(iReturn)
	{
		printf("VSLab client: Feed Server Error\n");
		return -1;
	}

	// close library and close open files
	vslcl_Close();

	gettimeofday(&end, NULL);
	printf("Completed in %.2f s\n", (difftime(end.tv_sec, begin.tv_sec)+difftime(end.tv_usec, begin.tv_usec)/1000000));
	return 0;
}

/**
 *	\}
 */
