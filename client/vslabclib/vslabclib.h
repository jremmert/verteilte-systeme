/**
 *	\file vslabclib.h
 *	\brief Definitions for vslab client lib
 *	\author Marc Juettner, marc.juettner@juettner-itconsult.de
 *	\author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *	\version 1.0
 *
 */
#if !defined _vslabclib_h_
#define _vslabclib_h_

#include "../../packetlib/packetlib.h"
#include "../../timeoutlib/timeoutlib.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

//#define DEBUG


// defines for use within the vslab client library...

/** \brief Default server port. 
 *
 * The port of the vslab server.
 */
#define VSLS_PORT		11111

/** \brief Default server IP. 
 *
 * The default IP of the vslab server.
 */
#define BROADCAST_ADDRESS	"141.47.95.255"

/** \brief IP address length. 
 *
 * Number of bytes to be reserved for server IP address.
 */
#define IP_ADDR_LEN		16


// vslab client library states
/** \brief Library status. 
 *
 * vslab client library status is set to 1 when the socket interface is opened and bound.
 */
#define VSLCL_STATUS_OFF	0
#define VSLCL_STATUS_ON		1


// error codes of vslabclib functions
/** \brief No error. */
#define EVSLCL_NOERROR	0

/** \brief Bind error. 
 *
 * An error occurred during socket binding.
 */
#define EVSLCL_BIND		101

/** \brief Socket error. 
 *
 * An error occured while creating a socket.
 */
#define EVSLCL_SOCKET		102

/** \brief Library not opened.  
 *
 * A library function was called without previous call to vslcl_Open().
 */
#define EVSLCL_STATUS_OFF	103

/** \brief Unknown error.  
 *
 * An unknown error happened.
 */
#define EVSLCL_UNKNOWN_ERROR	104

/** \brief Network timeout error.  
 *
 * Network timed out.
 */
#define EVSLCL_NET_TIMEOUT	105

/** \brief Null pointer argument passed to function call.  
 *
 * A null pointer was passed to a function.
 */
#define EVSLCL_NULLPTR	106
		

/** \brief Wrong address length.  
 *
 * The given IP address doesn't have the correct length.
 */
#define EVSLCL_WRONGADDRLEN	107

/** \brief Library already opened.  
 *
 * There was a second call to vslcl_Open().
 */
#define EVSLCL_STATUS_ON	108

//##########################################
//##########################################
//##########################################

#define EXIT_TIMEOUT 2
#define EXIT_NOW_NO_PACKET 3
#define EXIT_GP_PACKET 4
#define EXIT_END_OF_FILE 5
#define EXIT_BROADCAST_PACKET 6
#define EXIT_UNKNOWN_PACKETTYPE 7
#define EXIT_NO_NEW_SERVER 8
#define EXIT_NO_NEW_CHAR 9
#define EXIT_DATA_PACKET 10
#define EXIT_READ_UNSENT_DATA 11

#define SOCKET_RECEIVE_TIMEOUT 1		// Timeout for Packet receive, in s

//#define SLOWDOWN

#define MAX_SERVER 15


#define READY	1
#define BUSY	0

#ifndef TRUE
#define TRUE 	1
#endif

#ifndef FALSE
#define FALSE	0
#endif

enum feedServerStates {
	sm_start,
	setGeneratorPolynom,
	readData,
	sendData,
	receiveData,
	evaluateData,
	writeData,
	unlock,
	error,
	sm_exit,
};

enum errorStates {
	timeout,
	PacketReceivedtoEvaluateError,
	UnknownPackettype,
	pollSocketError,
	doNothing,
	unlockError,
	sendDataError,
	readDataError,
	setGeneratorPolynomError
};


struct errorStatuses {
	enum errorStates errorState;
	int timeoutCount;
};

struct unsentData {
	int txtPosition[20];		// stores txt_posiiton
	struct pl_data data[20];
	int count;
};

struct server_struct {

	//States
	enum feedServerStates state;
	struct errorStatuses errorStatus;

	//Broadcast
	bool broadcast;
	in_addr_t broadcastResponse;
	int broadcastRetry;
	int sendBroadcastCount;

	// Transmit
	struct pl_data data;
	struct sockaddr_in remote;
	time_t timestamp;

	//Receive
	unsigned int receive_packet_len;
	uint8_t receive_packet[PL_PACKETSIZE];
	int retrys;

	//Write data
	int txt_position;
	bool end_of_file;

	// Postbox
	bool newMessage;
	uint8_t waiting_receive_packet[PL_PACKETSIZE];		// here a new packet, received in an other server_list, is stored until this server_list reaches pollSocket method
};

struct broadcast_struct {
	//Socket
	struct sockaddr_in 	local,
						remote;

	//Transmit
	struct pl_data data;
	time_t timestamp;
};


struct ip_struct {
	in_addr_t adress;
};

struct server_list_struct {
	//int count;
	struct ip_struct ip[MAX_SERVER];
	int count;
};


// vslab client library function prototypes
void vslcl_SetClientID(int id);
void vslcl_SetPrio(int prio);
void vslcl_SetServercount(int count);

int vslcl_OpenFile(char *encryptTxt,char *dencryptTxt);
int vslcl_ExtractGeneratorPolynom(void);


int vslcl_Open(void);
int vslcl_Init_server(void);

int vslcl_FeedServer(void);

int vslcl_Close(void);
int vslcl_SetUnicastAddress(char *address);

int vslcl_transmitGeneratorPolynom(int op1, int servernumber);
int vslcl_decryptSymbol(int op1, int *result);
int vslcl_sendSymbols(int op[15][64]);
int vslcl_receiveSymbols(int result[15][64]);
int vslcl_Unlock(void);
int vslcl_ScrambleGPn(int GP, int *datain, int *dataout, unsigned int dlen, char **addresses);
#endif //#define _vslabclib_h_
