/**
 *	\file vslabclib.c 
 *	\author Marc Juettner, marc.juettner@juettner-itconsult.de
 *	\author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *	\version 1.0
 *	\brief A library that implements access to a remote vslab server
 *	
 *	\defgroup vslabclib VSLab client library
 *	\{
 * \par Prerequisites
 * This library needs a running vslab server deamon on a properly configured hardware platform 
 * before running any application that uses it.
 *
 * \warning This library is not thread-safe!
 * It is impossible to access the vslab client library from multiple applications 
 * simultaneously as multiple parallel calls to vslcl_call_function() would cause 
 * received packets not to be assigned properly to the process or thread that expects it. 
 * There is much to do to make this library useable in multithreaded applications!
 * \}
 */
#include "vslabclib.h"

/**
 *	\ingroup vslabclib
 *	\defgroup vslcl_statvar Static variables
 *	\{
 */

/**
 *	Declaration of static variables that work across library functions - don't attempt to 
 *	use them outside the library!
 */

/**
 *	\brief The socket descriptor for remote vslab server access.
 *
 *	iVSLSocket will hold the socket descriptor returned by socket() to easily access
 *	the remote device once it is opened. Remember that the socket descriptor is not the only
 *	way to identify a remote system - here the socket descriptor is used only as a 
 *	descriptor refering to ONE socket that is used for any server the library communicates 
 *	with.
 *
 */
static int iDataSocket = 0, iBroadcastSocket = 0;

/**
 *	\brief The library status.
 *
 * 	iVSLCLStatus will hold information on the library's operating status. Any function 
 *	that requires proper initialization of the library will first check this variable and 
 *	return with an error code if accessing the library would create an error.
 *
 */
static int iVSLCLStatus = VSLCL_STATUS_OFF;

/**
 *	\brief Local socket information.
 *
 *	localDataAddr holds information about the local system.
 */
static struct sockaddr_in	localDataAddr, localBroadcastAddr, remoteBroadcastAddr;


//##########################################
//VARIABLE SPACE for Project extension
//##########################################

static struct server_struct server_info[MAX_SERVER];

static struct server_list_struct server_list;

static struct unsentData unsentDataStatus;

static int iClientId,
			iPrio,
			iServerCount,
			iCounterTxt = 0;

static time_t timestampBroadcast;

static 	FILE *cryptTxt, *outTxt;

static 	uint16_t iGP;

//##########################################
//Intern Functions
//##########################################

void vslcl_SetClientID(int id)
{
	iClientId = id & 0xFF;
}


void vslcl_SetPrio(int prio)
{
	iPrio = prio & 0xFF;
}


void vslcl_SetServercount(int count)
{
	iServerCount = count & 0xFF;
}


int setupServerAdresses(void)
{
	return EXIT_SUCCESS;
}


int readSymbol(FILE *cryptTxt)
{
	int tmp0, tmp1;

	tmp1 = fgetc(cryptTxt);

	if(tmp1 != EOF)
	{
		tmp0 = tmp1;
		tmp0 = tmp0 << 8;

		tmp1 = fgetc(cryptTxt);
		if(tmp1 == EOF)
		{
			printf("Error during file read: Char missing in File\n");
			return EOF;
		}
		tmp0 = tmp0 | tmp1;

		return tmp0;
	}

	return EOF;
}


int sendBroadcast(void)
{
	static uint8_t 	sndpacket[PL_PACKETSIZE];
	struct pl_data broadcastData;

	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	// set function id
	PLM_FUNCTION_ID(broadcastData) = BROAD;

	// set data length
	PLM_LENGTH_PAYLOAD(broadcastData) = 0;

	// create request packet...
	pl_create_request(&broadcastData, iClientId, iPrio);

	// set ProtocolVersion
	PLM_PROT_VERSION(broadcastData) = PL_VERSION;

	// serialize packet
	pl_make_packet(&broadcastData, sndpacket, PL_PACKETSIZE);

	sendto(iBroadcastSocket, &broadcastData, PL_PACKETSIZE, 0, (struct sockaddr*)&remoteBroadcastAddr, sizeof(struct sockaddr));

	return EXIT_SUCCESS;
}


int setDataGeneratorPolynom(int servernumber)
{
	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	// set operands
	PLM_OPERAND(server_info[servernumber].data, 0) = iGP;

	// set function id
	PLM_FUNCTION_ID(server_info[servernumber].data) = SET_GEN;

	// set data length
	PLM_LENGTH_PAYLOAD(server_info[servernumber].data) = 1;

	return EXIT_SUCCESS;
}


int setDataSymbols(int servernumber)
{
	int i = 0,
		tmp = 0,
		unsentTxtPosition = 0;
	bool setFilepointerback = false;

	if(unsentDataStatus.count > 0)
	{
		unsentTxtPosition = unsentDataStatus.txtPosition[unsentDataStatus.count-1];

		fseek(cryptTxt, (unsentTxtPosition + 1) * 2, SEEK_SET);

		// indicates to set filepointer at the end of the read process to the original value
		setFilepointerback = true;

		server_info[servernumber].txt_position = unsentTxtPosition;

		printf("------%d: unsendDataStatus.count: %d  fp: %d  --------\n", servernumber, unsentDataStatus.count, unsentDataStatus.txtPosition[unsentDataStatus.count-1]);
		unsentDataStatus.count--;

		//memcpy(&server_info[servernumber].data, &unsentDataStatus.data[unsentDataStatus.count], PL_PACKETSIZE);
		server_info[servernumber].txt_position = unsentDataStatus.txtPosition[unsentDataStatus.count];

		//return EXIT_SUCCESS;
	}
	else
	{
		server_info[servernumber].txt_position = iCounterTxt;
	}

	for (i = 0; i < 64; i++)
	{
		tmp = readSymbol(cryptTxt);

		if (tmp != EOF)
		{
			PLM_OPERAND(server_info[servernumber].data, i) = tmp;

			if(setFilepointerback == false)
			{
				iCounterTxt++;
			}
		}
		else
		{
			server_info[servernumber].end_of_file = true;
			break;
		}
	}

	if(i == 0)
	{
		printf("%d: setDataSymbols | EXIT_NO_NEW_CHAR \n",servernumber);
		return EXIT_NO_NEW_CHAR;
	}

	// set function id
	PLM_FUNCTION_ID(server_info[servernumber].data) = DATA_DECRYPT;
	// set data length
	PLM_LENGTH_PAYLOAD(server_info[servernumber].data) = i;

	if(setFilepointerback == true)
	{
		setFilepointerback = false;
		fseek(cryptTxt, (iCounterTxt+ 1) * 2, SEEK_SET);
	}

	return EXIT_SUCCESS;
}


int setDataUnlockServer(int servernumber)
{
	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	// set function id
	PLM_FUNCTION_ID(server_info[servernumber].data) = RELEASE_SERVER;

	// set data length
	PLM_LENGTH_PAYLOAD(server_info[servernumber].data) = 0;

	return EXIT_SUCCESS;
}

int transmitData(int servernumber)
{
	static uint8_t 	sndpacket[PL_PACKETSIZE];

	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	// create request packet...
	pl_create_request(&server_info[servernumber].data, iClientId, iPrio);

	// set ProtocolVersion
	PLM_PROT_VERSION(server_info[servernumber].data) = PL_VERSION;

	// serialize packet
	pl_make_packet(&server_info[servernumber].data, sndpacket, PL_PACKETSIZE);

	// send packet
	sendto(iDataSocket, &sndpacket, PL_PACKETSIZE, 0, (struct sockaddr*)&server_info[servernumber].remote, sizeof(struct sockaddr));

	server_info[servernumber].timestamp = time(NULL);

	return EXIT_SUCCESS;
}


int copyNewIpAdress(int servernumber)
{
	int i = 0;
	bool server_exist = false;

	for(i=0;i<=server_list.count;i++)
	{
		if(server_list.ip[i].adress == server_info[servernumber].broadcastResponse)
		{
			server_exist = true;
		}
	}

	if(server_exist == false)
	{
		//IP nicht vorhanden
		server_info[servernumber].remote.sin_addr.s_addr = server_info[servernumber].broadcastResponse;
		server_list.ip[server_list.count].adress = server_info[servernumber].broadcastResponse;

		printf("%d: Server IP: %d added to list\n",servernumber,(server_list.ip[server_list.count].adress & 0xff000000)>>24);
		server_list.count++;

		return EXIT_SUCCESS;
	}

	return EXIT_FAILURE;
}


int pollDataSocket(int servernumber)
{
	int i = 0, tmp_receive_packet_len = 0;
	struct sockaddr_in tmp_remote;
	uint8_t tmp_receive_packet[PL_PACKETSIZE];
	unsigned int socketlength = 0;

	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	socketlength = sizeof(struct sockaddr);

	//Check if Sockets stored a received message
	if(server_info[servernumber].broadcast == false)
	{
		tmp_receive_packet_len = recvfrom(iDataSocket, &tmp_receive_packet, PL_PACKETSIZE, MSG_DONTWAIT, (struct sockaddr*)&tmp_remote, &socketlength);
	}

	if(server_info[servernumber].broadcast == true)
	{
		tmp_receive_packet_len = recvfrom(iBroadcastSocket, &tmp_receive_packet, PL_PACKETSIZE, MSG_DONTWAIT, (struct sockaddr*)&tmp_remote, &socketlength);
	}


	if(tmp_receive_packet_len == PL_PACKETSIZE)		// if packet was received, copy to the designated server_info
	{
		if(server_info[servernumber].broadcast == false)
		{
			for(i=0; i < MAX_SERVER; i++)
			{
				if((server_info[i].remote.sin_addr.s_addr == tmp_remote.sin_addr.s_addr) && (server_info[i].newMessage == false))
				{
					memcpy(&server_info[i].waiting_receive_packet, &tmp_receive_packet, PL_PACKETSIZE);
					server_info[i].receive_packet_len = tmp_receive_packet_len;
					server_info[i].newMessage = true;
				}
			}
		}

		if(server_info[servernumber].broadcast == true)
		{
			server_info[servernumber].broadcastResponse = tmp_remote.sin_addr.s_addr;

			memcpy(&server_info[servernumber].receive_packet, &tmp_receive_packet, PL_PACKETSIZE);
			server_info[servernumber].receive_packet_len = tmp_receive_packet_len;

			if(copyNewIpAdress(servernumber) == EXIT_FAILURE) return EXIT_NO_NEW_SERVER;

			return EXIT_SUCCESS;
		}

	}

	if(server_info[servernumber].newMessage == true)
	{	// Fetch new Message and mark for no new messages
		memcpy(&server_info[servernumber].receive_packet, &server_info[servernumber].waiting_receive_packet, PL_PACKETSIZE);
		server_info[servernumber].newMessage = false;
		server_info[servernumber].errorStatus.timeoutCount = 0;		// If packet was received successful, reset timeoutCount
		return EXIT_SUCCESS;
	}

	if(difftime(time(NULL), server_info[servernumber].timestamp) > SOCKET_RECEIVE_TIMEOUT)
	{
		return EXIT_TIMEOUT;
	}

	else return EXIT_NOW_NO_PACKET;
}


int checkDataResponse(int servernumber)
{
		// extract packet received
		pl_extr_packet(server_info[servernumber].receive_packet, &server_info[servernumber].data, server_info[servernumber].receive_packet_len);

		switch(PLM_PACKET_TYPE(server_info[servernumber].data))
		{
		case PL_PTYPE_RSP:
			if(PLM_FUNCTION_ID(server_info[servernumber].data) == BROAD)
			{
				server_info[servernumber].broadcast = false;
				return EXIT_BROADCAST_PACKET;
			}
			else if(PLM_FUNCTION_ID(server_info[servernumber].data) == SET_GEN)
			{
				return EXIT_GP_PACKET;
			}
			else if(PLM_FUNCTION_ID(server_info[servernumber].data) == DATA_DECRYPT)
			{
				server_info[servernumber].retrys = 0;
				return EXIT_DATA_PACKET;
			}

			printf("%d: checkDataResponse PacketType: %d Fkt_ID: %d\n",servernumber,PLM_PACKET_TYPE(server_info[servernumber].data), PLM_FUNCTION_ID(server_info[servernumber].data));
			return EXIT_UNKNOWN_PACKETTYPE;
			break;

		case PL_PTYPE_ERR:
			return EXIT_FAILURE;
			break;

		default:
			printf("%d: checkDataResponse PacketType: %d Fkt_ID: %d\n",servernumber,PLM_PACKET_TYPE(server_info[servernumber].data), PLM_FUNCTION_ID(server_info[servernumber].data));
			return EXIT_UNKNOWN_PACKETTYPE;
			break;
		}
	return EXIT_FAILURE;
}


int writeDataSymbolsInFile(int servernumber)
{
	int i = 0;

	fseek(outTxt, server_info[servernumber].txt_position, SEEK_SET);

	for (i=0; i < PLM_LENGTH_PAYLOAD(server_info[servernumber].data); i++)
	{
		fputc(PLM_OPERAND(server_info[servernumber].data, i), outTxt);
	}

	if(server_info[servernumber].end_of_file == true)
	{
		printf("%d: #writeDataSymbolsInFile# EOF reached...exit\n", servernumber);
		return EXIT_END_OF_FILE;
	}

	return EXIT_SUCCESS;
}


int unlockServer(int servernumber)
{
	int k = 0, n = 0, allReady = true;

	for (k=0; k < iServerCount; k++)
	{
		//printf("Server: %d state: %d errorState: %d\n", k ,server_info[k].state ,server_info[k].errorStatus.errorState);
		switch(server_info[k].state)
		{
		case unlock:

			break;

		case error:
			if(server_info[k].errorStatus.errorState != doNothing)
			{
				allReady = false;
			}

			break;

		default:
			allReady = false;
			break;
		}
	}

	if(allReady == true)
	{
		if(unsentDataStatus.count > 0)
		{
			printf("\n \n SEARCH NEW SERVER \n");
			vslcl_Init_server();
			sendBroadcast();
			return EXIT_SUCCESS;
		}

		for(n=0; n < iServerCount; n++)	// Release all Servers
		{

			if(setDataUnlockServer(n) == EXIT_SUCCESS)
			{
				transmitData(n);
				server_info[n].state = sm_exit;
			}
			else
			{
				return EXIT_FAILURE;
			}
		}
		return EXIT_SUCCESS;
	}



	return EXIT_FAILURE;
}


int errorHandling(int servernumber)
{
	switch (server_info[servernumber].errorStatus.errorState)
					{
					case timeout:
						server_info[servernumber].errorStatus.timeoutCount ++;
						server_info[servernumber].retrys = 3 - server_info[servernumber].errorStatus.timeoutCount;

						if(server_info[servernumber].retrys < 1)
						{
							printf("%d: Server with IP: %d dropped out, not answering\n",servernumber,((server_info[servernumber].remote.sin_addr.s_addr&0xff000000)>>24));

							unsentDataStatus.txtPosition[unsentDataStatus.count] = server_info[servernumber].txt_position;
							memcpy(&unsentDataStatus.data[unsentDataStatus.count], &server_info[servernumber].data, PL_PACKETSIZE);

							unsentDataStatus.count++;

							// mark actual server as bad
							server_info[servernumber].errorStatus.errorState = doNothing;
							server_info[servernumber].state=error;		// provisionally this bad server remains in the error state
						}
						else
						{
							printf("%d: Timeout occurred, try it %d times more.\n",servernumber , server_info[servernumber].retrys);
							server_info[servernumber].state = sendData;
						}
						break;

					case PacketReceivedtoEvaluateError:
						printf("%d: Error packet received from server %d | ",servernumber,(server_info[servernumber].remote.sin_addr.s_addr&0xff000000)>>24); // this error message will be continued in separate function no \n

						switch (server_info[servernumber].data.operand[0])
						{
						case PL_ERR_GENERALERROR:
							printf(" PL_ERR_GENERALERROR\n");
							break;
						case PL_ERR_INVALIDTYPE:
							printf(" PL_ERR_INVALIDTYPE\n");
							break;
						case PL_ERR_INVALID_PROT_VERSION:
							printf(" PL_ERR_INVALID_PROT_VERSION\n");
							break;
						case PL_ERR_FUNCEXECERROR:
							printf(" PL_ERR_FUNCEXECERROR\n");
							break;
						case PL_ERR_NOSUCHFUNCTION:
							printf(" PL_ERR_NOSUCHFUNCTION\n");
							break;
						case PL_ERR_HIGHER_PRIO_ACTIVE:
							printf("PL_ERR_HIGHER_PRIO_ACTIVE\n");
							if(PLM_FUNCTION_ID(server_info[servernumber].data) == DATA_DECRYPT)
							{
								printf("%d: Packet lost, higher prio: unsendDataStatus.count: %d\n", servernumber, unsentDataStatus.count);

								unsentDataStatus.txtPosition[unsentDataStatus.count] = server_info[servernumber].txt_position;
								memcpy(&unsentDataStatus.data[unsentDataStatus.count], &server_info[servernumber].data, PL_PACKETSIZE);

								unsentDataStatus.count++;
							}
							break;
						default:
							printf("Unknown Error\n");
							break;
						}

						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;

						break;

					case pollSocketError:
						printf("%d: pollSocketError\n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;

					case UnknownPackettype:
						printf("%d: Received an Unknown Packet from %d -> Server Blocked \n",servernumber,(server_info[servernumber].broadcastResponse & 0xff000000)>>24);

						server_info[servernumber].state = error;
						server_info[servernumber].errorStatus.errorState = doNothing;
						break;

					case doNothing:
						{
							int k = 0, n = 0;
							bool eofReached = false, allReady = true;

							for (k=0; k < iServerCount; k++)
							{
								if(server_info[k].end_of_file == true)
								{
									eofReached = true;
								}

								switch(server_info[k].state)
								{
								case error:
									if(server_info[k].errorStatus.errorState != doNothing)
									{
										allReady = false;
									}

									break;

								default:
									allReady = false;
									break;
								}
							}

							if(allReady == true)
							{
								if((unsentDataStatus.count > 0) || (eofReached == false))
								{
									printf("\n \n SEARCH NEW SERVER \n");
									if(difftime(time(NULL), timestampBroadcast) > 1){
										sendBroadcast();
										vslcl_Init_server();
										timestampBroadcast = time(NULL);
									}

									printf("All Server paused but unsent Data exist so restart the program\n");
								}
								else
								{
									for(n=0; n < iServerCount; n++)	// Release all Servers
									{
										server_info[n].state = sm_exit;
									}
									printf("All Server paused so exit the program\n");
								}
							}
						}
						break;

					case unlockError:
						printf("%d: Unlock Error\n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;

					case sendDataError:
						printf("%d: Send Data Error\n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;

					case readDataError:
						printf("%d: Read Data Error\n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;

					case setGeneratorPolynomError:
						printf("%d: Set Generator Polynom Error \n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;

					default:
						printf("%d:Unknown error in error handling\n",servernumber);
						server_info[servernumber].errorStatus.errorState = doNothing;
						server_info[servernumber].state=error;
						break;
					}

	return EXIT_SUCCESS;
}


//##########################################
//Extern Functions
//##########################################

/**
 *	\brief Initialize vslab client library.
 *	\return Zero if successfully opened socket, error code otherwise
 *
 *	vslcl_Open() will start initialization of the vslab client library and should be called at 
 *	the beginning of any program that wants to access the remote node. vslcl_Open() will  
 *	return zero if successfully executed and an error code less than zero otherwise.
 */
int vslcl_Open(void)
{
	int iReturn = 0	, iOne = 1;
	char tmpAdress[IP_ADDR_LEN];

	strcpy(tmpAdress, BROADCAST_ADDRESS);
	

	//get socket descriptor
	iDataSocket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	iBroadcastSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (iDataSocket < 0 || iBroadcastSocket < 0)
	{
		iVSLCLStatus = VSLCL_STATUS_OFF;
		return EXIT_FAILURE;
	}


   	// initialize client structure - that's me
   	localDataAddr.sin_family = AF_INET;			// Ethernet
   	localDataAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// automatically insert own address
   	localDataAddr.sin_port = htons(0);			// set vslab server port
   	memset(&(localDataAddr.sin_zero), 0x00, 8);		// set remaining bytes to 0x0

	localBroadcastAddr.sin_family = AF_INET;			// Ethernet
	localBroadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// automatically insert own address
	localBroadcastAddr.sin_port = htons(0);			// set vslab server port
   	memset(&(localBroadcastAddr.sin_zero), 0x00, 8);		// set remaining bytes to 0x0

   	remoteBroadcastAddr.sin_family = AF_INET;			// Ethernet
   	remoteBroadcastAddr.sin_addr.s_addr = inet_addr(tmpAdress);
   	remoteBroadcastAddr.sin_port = htons(VSLS_PORT);
	memset(&(remoteBroadcastAddr.sin_zero), 0x00, 8);


   	// bind socket
	iReturn = bind(iDataSocket, (struct sockaddr *)&localDataAddr, sizeof(struct sockaddr));
	iReturn = bind(iBroadcastSocket, (struct sockaddr *)&localBroadcastAddr, sizeof(struct sockaddr));
	if (iReturn < 0)
	{
		close(iDataSocket);
		close(iBroadcastSocket);
		iVSLCLStatus = VSLCL_STATUS_OFF;
		return EXIT_FAILURE;
	}

	setsockopt(iBroadcastSocket,SOL_SOCKET,SO_BROADCAST,&iOne,sizeof(iOne));

	// set library status
	iVSLCLStatus = VSLCL_STATUS_ON;
	unsentDataStatus.count = 0;

	return EXIT_SUCCESS;
}


int vslcl_Init_server(void) {
	int i = 0;

	for(i = 0; i < MAX_SERVER;i++)
	{
		server_info[i].remote.sin_family = AF_INET;
		server_info[i].remote.sin_port = htons(VSLS_PORT);
		memset(&(server_info[i].remote.sin_zero), 0x00, 8);

		server_info[i].state = sm_start;
		server_info[i].broadcast = true;
		server_info[i].broadcastRetry = 0;
		server_info[i].receive_packet_len = 0;
		server_info[i].retrys = 0;
		server_info[i].end_of_file = false;
		server_info[i].newMessage = false;
		server_info[i].errorStatus.timeoutCount = 0;
		server_info[i].sendBroadcastCount = 0;
		server_info[i].remote.sin_addr.s_addr = 0;

		PLM_CLIENT_ID(server_info[i].data) = 0;
		PLM_FUNCTION_ID(server_info[i].data) = 0;
		PLM_LENGTH_PAYLOAD(server_info[i].data) = 0;
		PLM_PACKET_TYPE(server_info[i].data) = 0;
		PLM_PRIORITY(server_info[i].data) = 0;
		PLM_PROT_VERSION(server_info[i].data) = 0;

		server_list.ip[i].adress = 0;
	}

	server_list.count = 0;
	return EXIT_SUCCESS;
}


int vslcl_OpenFile(char *encryptTxt,char *dencryptTxt)
{
	cryptTxt = fopen(encryptTxt, "r");
	if(NULL == cryptTxt) {
	      printf("Could not open \"%s\"!\n", encryptTxt);
	      return EXIT_FAILURE;
	   }

	outTxt = fopen(dencryptTxt, "w");
	if(NULL == outTxt) {
	      printf("Could not open \"%s\"!\n", dencryptTxt);
	      return EXIT_FAILURE;
	   }

	return EXIT_SUCCESS;
}


int vslcl_ExtractGeneratorPolynom(void)
{
	iGP = readSymbol(cryptTxt);
	return iGP;
}


int vslcl_FeedServer(void)
{
	int j = 0;

	if(sendBroadcast() == EXIT_FAILURE) return EXIT_FAILURE;

	while(1)
	{
		for(j = 0; j < iServerCount;j++)
		{
			switch(server_info[j].state)
			{
			case sm_start:
				server_info[j].state = receiveData;
				server_info[j].timestamp = time(NULL);
				break;

			case setGeneratorPolynom:
				if(setDataGeneratorPolynom(j) == EXIT_SUCCESS)
				{
					server_info[j].state = sendData;
				}
				else
				{
					server_info[j].errorStatus.errorState = setGeneratorPolynomError;
					server_info[j].state = error;
				}
				break;

			case readData:
				switch(setDataSymbols(j))
				{
				case EXIT_SUCCESS:
					server_info[j].state = sendData;
					break;

				case EXIT_NO_NEW_CHAR:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = doNothing;
					break;

				case EXIT_FAILURE:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = readDataError;
				break;
				}
				break;

			case sendData:
				switch(transmitData(j))
				{
				case EXIT_SUCCESS:
					server_info[j].state = receiveData;
					break;

				case EXIT_FAILURE:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = sendDataError;
					break;
				}
				break;

			case receiveData:
				switch(pollDataSocket(j))
				{
				case EXIT_SUCCESS:
					server_info[j].state = evaluateData;
					break;

				case EXIT_NOW_NO_PACKET:
					server_info[j].state = receiveData;
					break;

				case EXIT_TIMEOUT:
					if(server_info[j].broadcast==true)
					{
						server_info[j].errorStatus.errorState = doNothing;
						server_info[j].state = error;
					}
					else
					{
						server_info[j].errorStatus.errorState = timeout;
						server_info[j].state = error;
					}
					break;

				case EXIT_NO_NEW_SERVER:
					server_info[j].errorStatus.errorState = doNothing;
					server_info[j].state = error;
					break;

				default:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = pollSocketError;
					break;
				}
				break;

			case evaluateData:
				switch(checkDataResponse(j))
				{
				case EXIT_DATA_PACKET:
					server_info[j].state = writeData;
					break;

				case EXIT_FAILURE:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = PacketReceivedtoEvaluateError;
					break;

				case EXIT_GP_PACKET:
					server_info[j].state = readData;
					break;

				case EXIT_BROADCAST_PACKET:
					server_info[j].state = setGeneratorPolynom;
					break;

				case EXIT_UNKNOWN_PACKETTYPE:
					server_info[j].state = error;
					server_info[j].errorStatus.errorState = UnknownPackettype;
					break;
				}
				break;

			case writeData:
				switch(writeDataSymbolsInFile(j))
				{
				case EXIT_SUCCESS:
					server_info[j].state = readData;

					break;

				case EXIT_END_OF_FILE:
					server_info[j].state = unlock;
					break;
				}
				break;

			case unlock:
				if(unlockServer(j) == EXIT_SUCCESS)
				{
				}
				else
				{
					server_info[j].state = unlock;
				}
				break;

			case error:
				if(unsentDataStatus.count>999){
					printf("unsentDataStatus Array exceeds the value of 999 ... exit programm\n");
					return EXIT_FAILURE;
				}

				if(errorHandling(j) == EXIT_FAILURE)
				{
					return EXIT_FAILURE;
				}
				break;

			case sm_exit:
				//send release packet once
				return EXIT_SUCCESS;
				break;

			default:
				return EXIT_FAILURE;
			}
		}

		if(j == iServerCount)
		{
			j = 0;
		}
	}
	return EXIT_FAILURE;
}


int vslcl_Close(void)
{
	int iZero = 0;
	// check library status
	if (iVSLCLStatus != VSLCL_STATUS_ON) return -EVSLCL_STATUS_OFF;

	// close socket
	close (iDataSocket);
	setsockopt(iBroadcastSocket,SOL_SOCKET,SO_BROADCAST,&iZero,sizeof(iZero));
	close (iBroadcastSocket);

	// set library status
	iVSLCLStatus = VSLCL_STATUS_OFF;

	fclose(cryptTxt);
	fclose(outTxt);

	return EVSLCL_NOERROR;
}

/**
 *	\}
 */
