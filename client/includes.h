#if !defined _includes_h_
#define _includes_h_

//#define DEBUG

#define RETRY_COUNT 3

#include "vslabclib/vslabclib.h"
#include "vslabc.h"

//get required headers...
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#endif //#define _includes_h_
