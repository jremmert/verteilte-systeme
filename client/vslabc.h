/**
 *	\file vslabc.h
 *	\brief vslab client: General defines
 *	\author Marc Juettner, marc.juettner@juettner-itconsult.de
 *	\author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *	\version 1.0
 *
 */

#if !defined _vslabc_h_
#define _vslabc_h_

/** \brief vslab client version. 
 *
 * The version of the vslab client.
 */
#define VSLC_VERSION	"lab_1_template"

#endif //#define _vslabc_h_
