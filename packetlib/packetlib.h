/**
 *	\file packetlib.h
 *	\brief Definitions for packet handling
 *	\author Marc Juettner, marc.juettner@juettner-itconsult.de
 *	\author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *	\version 1.1
 *
 */
#if !defined _packetlib_h_
#define _packetlib_h_

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>

//Paket Version:
#define PL_VERSION     2

// packet types
/** \brief Request operation */
#define PL_PTYPE_REQ	1
/** \brief Respond to operation request */
#define PL_PTYPE_RSP	2
/** \brief Error packet */
#define PL_PTYPE_ERR	3


// function IDs
#define BROAD 1
#define SET_GEN 2
#define DATA_DECRYPT 3
#define RELEASE_SERVER 4

#define TEST 10

// error codes in server packets
/** \brief General error. */
#define PL_ERR_GENERALERROR	1
/** \brief Invalid packet type. */
#define PL_ERR_INVALIDTYPE	2
/** \brief Invalid packet mode. */
#define PL_ERR_INVALID_PROT_VERSION 3
/** \brief Function execution error. */
#define PL_ERR_FUNCEXECERROR	4
/** \brief No such function. */
#define PL_ERR_NOSUCHFUNCTION	5
/** \brief Higher priority is active. */
#define PL_ERR_HIGHER_PRIO_ACTIVE  6





// error codes of packetlib functions
/** \brief No error. */
#define E_PL_NOERROR			0
/** \brief NULL pointer. 
 * NULL pointer in function parameter.
 */
#define E_PL_NULLPTR			1
/** \brief Insufficient buffer size. 
 * Buffer size not sufficient.
 */
#define E_PL_INSUFFICIENTBUFFER		2


// indices for packet content byte adressing
#define PL_PIDX_PROT_VERS  0
#define PL_PIDX_TYPE	    1
#define PL_PIDX_PRIORITY   2
#define PL_PIDX_FID	        3
#define PL_PIDX_CLIENT_ID  4
#define PL_PIDX_DATA_LENGTH  5
#define PL_PIDX_OP(x)	(6 + 2*x)


// definitions for operand count and packet size
#define PL_OPERAND_COUNT	64
#define PL_PACKETSIZE		134//(sizeof(struct pl_data))



#define PLM_PACKET_TYPE(x)	x.packetType

#define PLM_CLIENT_ID(x)  x.clientId

#define PLM_PRIORITY(x)  x.priority


#define PLM_FUNCTION_ID(x)	x.functionId


#define PLM_OPERAND(x, y)	x.operand[y]

#define PLM_PROT_VERSION(x) x.protocollVersion

#define PLM_LENGTH_PAYLOAD(x) x.dataLength

/**
 *	\brief packet data structure
 *	This structure represents the core data structure of the protocol.
 */
struct pl_data {
	uint8_t protocollVersion;
	uint8_t packetType;
	uint8_t priority;
	uint8_t functionId;
	uint8_t clientId;
	uint8_t dataLength;
	uint16_t operand[PL_OPERAND_COUNT];	/**< \brief The packet's operands. */
};

// Function prototypes
int pl_make_packet(struct pl_data *, uint8_t *, unsigned int);
int pl_extr_packet(uint8_t*, struct pl_data *, unsigned int);
int pl_create_response(struct pl_data *);
int pl_create_request(struct pl_data *,uint8_t, uint8_t);
int pl_create_error(struct pl_data *, uint8_t);

#endif //#define _packetlib_h_
