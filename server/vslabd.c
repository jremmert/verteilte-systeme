/**
 *	\file vslabd.c
 *	\brief The VSLab daemon
 *	\author Marc Juettner, marc.juettner@juettner-itconsult.de
 *	\author Manuel Gaiser, manuel.gaiser@hs-pforzheim.de
 *	\version 2.0
 *
 *	\defgroup vslabdaemon VSLab daemon implementation
 *	\{
 */
#include "includes.h"




void lock(struct LockStatus *structPtr, struct pl_data *vsld_data){
	(*structPtr).currentLockID=(*vsld_data).clientId;
	(*structPtr).currentLockPriority=(*vsld_data).priority;
	(*structPtr).locked=LOCKED;
	printf("Server locked to ID: %d \n", (*structPtr).currentLockID);
}

void unlock(struct LockStatus *structPtr){
	(*structPtr).locked=UNLOCKED;
	printf("Server Released\n");
}




int main(void)
{
	int iReturn = 0;//iResult = 0;
	int iVSLSocket = 0;
	int iRcvLen = 0;
	unsigned int i, j;
	
	struct LockStatus CurrentLockStatus;
	CurrentLockStatus.currentLockPriority=255;
	CurrentLockStatus.locked=UNLOCKED;
	CurrentLockStatus.currentLockID=255;

	time_t lastPacket;

	struct pl_data vsld_data;
	
	struct sockaddr_in vsld_remote, vsld_local;
	uint8_t sndpacket[PL_PACKETSIZE];
	uint8_t rcvpacket[PL_PACKETSIZE];

	// introducing myself...
	printf("VSLab server daemon, version %s, build %s %s\n", VSLD_VERSION, __DATE__, __TIME__);

	InitTestingFunctions();
#ifdef DEBUG
	printf("TestingFunctions initialized\n");
#endif

	// get a socket descriptor from OS
	iVSLSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (iVSLSocket < 0)
	{
		perror("vslabd: Error creating socket.\n");
		return -ESOCKET;
	}

	// initialize server description structure - that's me
   	vsld_local.sin_family = AF_INET;			// Ethernet
   	vsld_local.sin_addr.s_addr = htonl(INADDR_ANY);		// automatically insert own address
   	vsld_local.sin_port = htons(VSLD_PORT);			// set vslab server port
   	memset(&(vsld_local.sin_zero), 0x00, 8);		// set remaining bytes to 0x0

	// initialize client description structure - in fact we don't need to do this as this struct
	// will be populated by recvfrom() calls.
	vsld_remote.sin_family = AF_INET;			// Ethernet
	vsld_remote.sin_addr.s_addr = htonl(INADDR_ANY);
	vsld_remote.sin_port = htons(VSLD_PORT);
	memset(&(vsld_remote.sin_zero), 0x00, 8);

	// bind socket
	iReturn = bind(iVSLSocket, (struct sockaddr *)&vsld_local, sizeof(struct sockaddr));
	if (iReturn < 0)
	{
		perror("vslabd: Error binding to socket.\n");
		close(iVSLSocket);
		return -EBIND;
	}

	// init scrambler
	unsigned int *reg_addr_gp;
	unsigned int *reg_addr_data;
	int decrypt_result;

	iReturn = PLREG_Open(PLREG_ADDR_GP, &reg_addr_gp);
	iReturn = PLREG_Open(PLREG_ADDR_DATA, &reg_addr_data);

	if (iReturn == EPLREG_NOERROR){
		printf("vslabd: Init scrambler successful...\n");
	}
	else{
		printf("vslabd: Init scrambler NOT successful...\n");
	}

	// main loop
	for (;;) {
		if((difftime(time(NULL), lastPacket) > TIMEOUT) && (CurrentLockStatus.locked==LOCKED)){
			unlock(&CurrentLockStatus);
		}

		// wait for incoming requests
		i = sizeof(struct sockaddr);
		if((CurrentLockStatus.locked==LOCKED)){
			tol_start_timeout(VSLD_TIMEOUT_SECS);
		}
		iRcvLen = recvfrom(iVSLSocket, &rcvpacket, PL_PACKETSIZE, 0, (struct sockaddr*)&vsld_remote, &i);
		tol_stop_timeout();
		if (tol_is_timed_out() && (CurrentLockStatus.locked==LOCKED)) {
			tol_reset_timeout();
			unlock(&CurrentLockStatus);
			continue;
		}

#ifdef MALFUNCTION_TIMEOUT
		malfunctionTest++;
		if(malfunctionTest > 100){
			malfunctionTest = 0;
			printf("Server stopped for testing.");
			continue;
		}
#endif

		// extract incoming packet		
		iReturn = pl_extr_packet(rcvpacket, &vsld_data, iRcvLen);

		// set a copy to the Testing-team
		AnalyzePacket(&vsld_data);

		// check for error during packet extraction
		if(iReturn < 0) {
			pl_create_error(&vsld_data, PL_ERR_GENERALERROR);
		}
		// check packet type
		else if (PLM_PACKET_TYPE(vsld_data) != PL_PTYPE_REQ) {
			pl_create_error(&vsld_data, PL_ERR_INVALIDTYPE);
		}
		// check protocol number. Has to be used, if Davids proposal is accepted; PL_VERSION actually missing
 		else if (PLM_PROT_VERSION(vsld_data) != PL_VERSION) {
			pl_create_error(&vsld_data, PL_ERR_INVALID_PROT_VERSION);
		}
 		else if ( PLM_FUNCTION_ID(vsld_data) == BROAD)
		{
//#ifdef DEBUG
				printf("Broadcast Packet received\n");
//#endif
				pl_create_response(&vsld_data);
		}
		// check if packet has sufficiently high priority
		else if ((PLM_PRIORITY(vsld_data) > CurrentLockStatus.currentLockPriority) && (CurrentLockStatus.locked==LOCKED)){
			pl_create_error(&vsld_data, PL_ERR_HIGHER_PRIO_ACTIVE);
			printf("Higher Prio, error 1\n");
		}
		// if a new client packet has the same priority, send error
		else if ((PLM_PRIORITY(vsld_data) == CurrentLockStatus.currentLockPriority) && (CurrentLockStatus.currentLockID!=vsld_data.clientId) && (CurrentLockStatus.locked==LOCKED)){
			pl_create_error(&vsld_data, PL_ERR_HIGHER_PRIO_ACTIVE);
			printf("Higher Prio, error: Received Prio: %d, Lock Prio: %d, Lock ID:  %d, Lock Status: %d\n", PLM_PRIORITY(vsld_data), CurrentLockStatus.currentLockPriority, CurrentLockStatus.currentLockID,CurrentLockStatus.locked);
		}

		else if ((vsld_data.functionId == DATA_DECRYPT) && (CurrentLockStatus.locked==LOCKED) && (vsld_data.clientId != CurrentLockStatus.currentLockID)){
			pl_create_error(&vsld_data, PL_ERR_GENERALERROR);
			printf("Generator polynom not set, but tried to decode\n");
		}

		// this is our core job - switch to the requested function id...
		else switch(PLM_FUNCTION_ID(vsld_data)) {
			case SET_GEN:
				// set the generator polynom
				printf("vslabd: Set GeneratorPolynom | Value: %x \n", PLM_OPERAND(vsld_data,0)) ;
				PLREG_SetGeneratorPolynom(reg_addr_gp, PLM_OPERAND(vsld_data,0));
				lock(&CurrentLockStatus, &vsld_data);
				pl_create_response(&vsld_data);
				lastPacket=time(NULL);
				break;
			case DATA_DECRYPT:
				//encrypt 16bit value and return a 8bit decrypted char
				for(j=0; j<vsld_data.dataLength; j++){
					PLREG_Scramble(reg_addr_data, PLM_OPERAND(vsld_data,j), &decrypt_result);
					PLM_OPERAND(vsld_data, j) = (uint8_t)decrypt_result;		// Evtl. pro 16bit nur ein rückgabechar 2*j
				}
#ifdef DEBUG
				printf("vslabd: Decrypt | Input Value: %x | Decrypt Value: %c \n", PLM_OPERAND(vsld_data,0), decrypt_result);
#endif
				pl_create_response(&vsld_data);
				//printf("%c",decrypt_result);
				lastPacket=time(NULL);
				break;
			case RELEASE_SERVER:
				// multiply operands 0 and 1 of the received packet
				unlock(&CurrentLockStatus);
				break;
			case TEST:
				BuildTestingResponse(&vsld_data);

#ifdef DEBUG
				//printf("Testing Response requested\n");
#endif
				break;
		default:
				// function is not implemented - create an error packet
#ifdef DEBUG
				printf("Function not implemented, error Packet sent\n");
#endif
				pl_create_error(&vsld_data, PL_ERR_NOSUCHFUNCTION);
				break;
		}

		// convert and send packet
		pl_make_packet(&vsld_data, sndpacket, PL_PACKETSIZE);
		// set a copy to the Testing-team
		AnalyzePacket(&vsld_data);
		sendto(iVSLSocket, &sndpacket, PL_PACKETSIZE, 0, (struct sockaddr*)&vsld_remote, sizeof(struct sockaddr));
	}
	return 0;
}

/**
 *	\}
 */
